function rotation_matrix_demo
clc
clear all
close all

	disp('Picking random Euler angles (radians)');

% 	x = 2*pi*rand() - pi % -180 to 180 (roll)
% 	y = pi*rand() - pi*0.5 % -90 to 90 (pitch)
% 	z = 2*pi*rand() - pi % -180 to 180 (yaw)

    x = 0;
    y = 0;
    z = pi/2;

	disp('\nRotation matrix is:');
	R = compose_rotation(x,y,z)
	
	disp('Decomposing R');
	[x2,y2,z2] = decompose_rotation(R)

	disp('');
	err = sqrt((x2-x)*(x2-x) + (y2-y)*(y2-y) + (z2-z)*(z2-z))

	if err < 1e-5
		disp('Results are correct!');
	else
		disp('Oops wrong results :(');
	end
end

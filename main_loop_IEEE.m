clc
clear all
close all

rmpath Euler2RotationMatrix/
rmpath QuadAnimation/
addpath Euler2RotationMatrix/
addpath QuadAnimation/

%%% To turn on the sensor noise, set this  value to 1
noise = 0;

%%
T = 20;
dt = 0.01; %%% Note: if the discretization step is 0.01, the system blows up!!
time = 0:dt:T;
sim_length = length(time);

%% Initializing vectors with zeros 

        ph_graph = zeros(sim_length,1);
        th_graph = zeros(sim_length,1);
        ps_graph = zeros(sim_length,1);
        p_graph = zeros(sim_length,1);
        q_graph = zeros(sim_length,1);
        r_graph = zeros(sim_length,1);
        x_graph = zeros(sim_length,1);
        y_graph = zeros(sim_length,1);
        z_graph = zeros(sim_length,1);
        vx_graph = zeros(sim_length,1);
        vy_graph = zeros(sim_length,1);
        vz_graph = zeros(sim_length,1);
        
        xi1_1_graph = zeros(sim_length,1);
        xi1_2_graph = zeros(sim_length,1);
        xi1_3_graph = zeros(sim_length,1);
        xi1_4_graph = zeros(sim_length,1);
        
        xi2_1_graph = zeros(sim_length,1);
        xi2_2_graph = zeros(sim_length,1);
        xi2_3_graph = zeros(sim_length,1);
        xi2_4_graph = zeros(sim_length,1);
        
        eta1_1_graph = zeros(sim_length,1);
        eta1_2_graph = zeros(sim_length,1);
        eta1_3_graph = zeros(sim_length,1);
        eta1_4_graph = zeros(sim_length,1);
        
        
        eta2_1_graph = zeros(sim_length,1);
        
        up_graph = zeros(sim_length,1);
        uq_graph = zeros(sim_length,1);
        ur_graph = zeros(sim_length,1);
        ut_graph = zeros(sim_length,1);
%% Initial conditions

K1 = 5.0*[ 1.7160    6.0260    7.9100    4.6000];
K2 = 1.5*[ 1.7160    6.0260    7.9100    4.6000];
K3 = 1.0*[1.7160    6.0260    7.9100    4.6000];

k1 = K1(1); %
k2 = K1(2);
k3 = K1(3);%
k4 = K1(4);

k5 = K2(1); %
k6 = K2(2);
k7 = K2(3);%
k8 = K2(4);

k9 = K3(1);
k10 = K3(2);
k11 = K3(3);
k12 = K3(4);

k13 = 1000;
k14 = 15;%%% not used in this particular case

L=1;
j11 = 0.0177;
j22 = 0.0177;
j33 = 0.0334;
g = 9.8;
m = 0.1;
kt = 0;
kr = 0;


% R_vec = reshape( R.', 1, 9 );

% R = diag([1,-1,-1]);
  ph = 170*(pi/180);
  th = 0*(pi/180);
  ps = 0*(pi/180);
  R = rotationZ(ps)*rotationY(th)*rotationX(ph);
 trace(R)

[ph,th,ps] = decompose_rotation(R);

x = 1.2;
y = 1.0;
z = 0;
vx = 0;
vy = 0;
vz = 0;
p = 0.0;
q=  0.0;
r = 0.0;
Omega = [p;q;r];
ze1 = 0.1;
ze2 = 0.1;

%  R_d = rotationZ(0*(pi/180))*rotationY(5*(pi/180))*rotationX(20*(pi/180));
 
    r11 = R(1,1);
    r12 = R(1,2);
    r13 = R(1,3);
    r21 = R(2,1);
    r22 = R(2,2);
    r23 = R(2,3);
    r31 = R(3,1);
    r32 = R(3,2);
    r33 = R(3,3);
    
    State = [x;y;z;vx;vy;vz;r11;r12,;r13;r21;r22;r23;r31;r32;r33;p;q;r;ze1;ze2];


%% Main simulation loop

for i = 1:T/dt -1

    %%%% Extracting state variables from the state vector
    x = State(1);
    y = State(2);
    z = State(3);
    vx = State(4);
    vy = State(5);
    vz = State(6);
    r11 = State(7);
    r12 = State(8);
    r13 = State(9);
    r21 = State(10);
    r22 = State(11);
    r23 = State(12);
    r31 = State(13);
    r32 = State(14);
    r33 = State(15);
    p = State(16);
    q = State(17);
    r = State(18);
    ze1 = State(19);
    ze2 = State(20);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%  Transformed states %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    xi1_1 = x^2 + y^2 - 1;
    xi1_2 = 2*vx*x + 2*vy*y;
    xi1_3 = -(2*(kt*vx*x - m*vy^2 - m*vx^2 + kt*vy*y + r13*x*ze1 + r23*y*ze1))/m;
    xi1_4 = (2*((kt*vx)/m + (r13*ze1)/m)*(kt*x - 2*m*vx))/m - (2*vy*(kt*vy + r23*ze1))/m - (2*vx*(kt*vx + r13*ze1))/m - (2*((kt*vy)/m + (r23*ze1)/m)*(2*m*vy - kt*y))/m - (2*ze2*(r13*x + r23*y))/m + (2*x*ze1*(p*r12 - q*r11))/m + (2*y*ze1*(p*r22 - q*r21))/m;
    Lg1Lf3S1 = -(2*(r13*x + r23*y))/m;
    Lg2Lf3S1 = ((2*r12*x*ze1)/m + (2*r22*y*ze1)/m)/j11;
    Lg3Lf3S1 = -((2*r11*x*ze1)/m + (2*r21*y*ze1)/m)/j22;
    Lg4Lf3S1 = 0;
    Lf4S1 = ((kt*vy)/m + (r23*ze1)/m)*((2*(kt*vy + r23*ze1))/m + (6*kt*vy)/m + (4*r23*ze1)/m + (2*kt*(2*m*vy - kt*y))/m^2) + (p*r12 - q*r11)*((2*vx*ze1)/m + (2*x*ze2)/m - (2*ze1*(kt*x - 2*m*vx))/m^2) + (p*r22 - q*r21)*((2*vy*ze1)/m + (2*y*ze2)/m + (2*ze1*(2*m*vy - kt*y))/m^2) - ze2*((2*r13*vx)/m + (2*r23*vy)/m - (2*r13*(kt*x - 2*m*vx))/m^2 + (2*r23*(2*m*vy - kt*y))/m^2 - (2*x*(p*r12 - q*r11))/m - (2*y*(p*r22 - q*r21))/m) + vx*((2*kt*((kt*vx)/m + (r13*ze1)/m))/m - (2*r13*ze2)/m + (2*ze1*(p*r12 - q*r11))/m) + vy*((2*kt*((kt*vy)/m + (r23*ze1)/m))/m - (2*r23*ze2)/m + (2*ze1*(p*r22 - q*r21))/m) + ((kt*vx)/m + (r13*ze1)/m)*((2*(kt*vx + r13*ze1))/m + (6*kt*vx)/m + (4*r13*ze1)/m - (2*kt*(kt*x - 2*m*vx))/m^2) + (((2*r11*x*ze1)/m + (2*r21*y*ze1)/m)*(kr*q + j11*p*r - j33*p*r))/j22 - (((2*r12*x*ze1)/m + (2*r22*y*ze1)/m)*(kr*p - j22*q*r + j33*q*r))/j11 + (2*p*x*ze1*(p*r13 - r*r11))/m + (2*p*y*ze1*(p*r23 - r*r21))/m + (2*q*x*ze1*(q*r13 - r*r12))/m + (2*q*y*ze1*(q*r23 - r*r22))/m;
    xi2_1 = z - 10;
    xi2_2 = vz;
    xi2_3 = -(kt*vz - g*m + r33*ze1)/m;
    xi2_4 = (ze1*(p*r32 - q*r31))/m - (r33*ze2)/m + (kt*((kt*vz)/m - g + (r33*ze1)/m))/m;
    Lg1Lf3S2 = -r33/m;
    Lg2Lf3S2 = (r32*ze1)/(j11*m);
    Lg3Lf3S2 = -(r31*ze1)/(j22*m);
    Lg4Lf3S2 = 0;
    Lf4S2 = ze2*((p*r32 - q*r31)/m + (kt*r33)/m^2) + (ze2/m - (kt*ze1)/m^2)*(p*r32 - q*r31) - (kt^2*((kt*vz)/m - g + (r33*ze1)/m))/m^2 + (p*ze1*(p*r33 - r*r31))/m + (q*ze1*(q*r33 - r*r32))/m + (r31*ze1*(kr*q + j11*p*r - j33*p*r))/(j22*m) - (r32*ze1*(kr*p - j22*q*r + j33*q*r))/(j11*m);
    eta1_1 = atan2(y, x);
    eta1_2 = (vy*x - vx*y)/(x^2 + y^2);
    eta1_3 = vx*(vy/(x^2 + y^2) - (2*x*(vy*x - vx*y))/(x^2 + y^2)^2) - vy*(vx/(x^2 + y^2) + (2*y*(vy*x - vx*y))/(x^2 + y^2)^2) - (x*((kt*vy)/m + (r23*ze1)/m))/(x^2 + y^2) + (y*((kt*vx)/m + (r13*ze1)/m))/(x^2 + y^2);
    eta1_4 = (kt^2*vy*x^5 - kt^2*vx*y^5 - 2*m^2*vy^3*x^3 + 2*m^2*vx^3*y^3 + 6*m^2*vx^2*vy*x^3 - 2*kt^2*vx*x^2*y^3 + 2*kt^2*vy*x^3*y^2 - 6*m^2*vx*vy^2*y^3 - 6*m^2*vx^3*x^2*y + 6*m^2*vy^3*x*y^2 + kt*r23*x^5*ze1 - kt*r13*y^5*ze1 - m*r23*x^5*ze2 + m*r13*y^5*ze2 - kt^2*vx*x^4*y + kt^2*vy*x*y^4 + m*p*r22*x^5*ze1 - m*p*r12*y^5*ze1 - m*q*r21*x^5*ze1 + m*q*r11*y^5*ze1 + 3*m*r23*vx*x^4*ze1 + 3*m*r13*vy*x^4*ze1 - kt*r13*x^4*y*ze1 + kt*r23*x*y^4*ze1 - 3*m*r23*vx*y^4*ze1 - 3*m*r13*vy*y^4*ze1 + m*r13*x^4*y*ze2 - m*r23*x*y^4*ze2 - 6*kt*m*vx^2*x*y^3 - 6*kt*m*vx^2*x^3*y + 6*kt*m*vy^2*x*y^3 + 6*kt*m*vy^2*x^3*y - 2*kt*r13*x^2*y^3*ze1 + 2*kt*r23*x^3*y^2*ze1 + 2*m*r13*x^2*y^3*ze2 - 2*m*r23*x^3*y^2*ze2 + 18*m^2*vx*vy^2*x^2*y - 18*m^2*vx^2*vy*x*y^2 + 6*kt*m*vx*vy*x^4 - 6*kt*m*vx*vy*y^4 - m*p*r12*x^4*y*ze1 + m*p*r22*x*y^4*ze1 + m*q*r11*x^4*y*ze1 - m*q*r21*x*y^4*ze1 - 6*m*r13*vx*x*y^3*ze1 - 6*m*r13*vx*x^3*y*ze1 + 6*m*r23*vy*x*y^3*ze1 + 6*m*r23*vy*x^3*y*ze1 - 2*m*p*r12*x^2*y^3*ze1 + 2*m*p*r22*x^3*y^2*ze1 + 2*m*q*r11*x^2*y^3*ze1 - 2*m*q*r21*x^3*y^2*ze1)/(m^2*(x^2 + y^2)^3);
    Lg1Lf3P1 = -(m*r23*x^5 - m*r13*y^5 - m*r13*x^4*y + m*r23*x*y^4 - 2*m*r13*x^2*y^3 + 2*m*r23*x^3*y^2)/(m^2*(x^2 + y^2)^3);
    Lg2Lf3P1 = (m*r22*x^5*ze1 - m*r12*y^5*ze1 - m*r12*x^4*y*ze1 + m*r22*x*y^4*ze1 - 2*m*r12*x^2*y^3*ze1 + 2*m*r22*x^3*y^2*ze1)/(j11*m^2*(x^2 + y^2)^3);
    Lg3Lf3P1 = -(m*r21*x^5*ze1 - m*r11*y^5*ze1 - m*r11*x^4*y*ze1 + m*r21*x*y^4*ze1 - 2*m*r11*x^2*y^3*ze1 + 2*m*r21*x^3*y^2*ze1)/(j22*m^2*(x^2 + y^2)^3);
    Lg4Lf3P1 = 0;
    Lf4P1 = vx*((5*kt^2*vy*x^4 + kt^2*vy*y^4 - 6*m^2*vy^3*x^2 + 6*m^2*vy^3*y^2 + 18*m^2*vx^2*vy*x^2 + 6*kt^2*vy*x^2*y^2 - 18*m^2*vx^2*vy*y^2 + 5*kt*r23*x^4*ze1 + kt*r23*y^4*ze1 - 5*m*r23*x^4*ze2 - m*r23*y^4*ze2 - 6*kt*m*vx^2*y^3 + 6*kt*m*vy^2*y^3 - 4*kt^2*vx*x*y^3 - 4*kt^2*vx*x^3*y - 12*m^2*vx^3*x*y + 5*m*p*r22*x^4*ze1 - 5*m*q*r21*x^4*ze1 + m*p*r22*y^4*ze1 - m*q*r21*y^4*ze1 + 12*m*r23*vx*x^3*ze1 + 12*m*r13*vy*x^3*ze1 - 4*kt*r13*x*y^3*ze1 - 4*kt*r13*x^3*y*ze1 - 6*m*r13*vx*y^3*ze1 + 6*m*r23*vy*y^3*ze1 + 4*m*r13*x*y^3*ze2 + 4*m*r13*x^3*y*ze2 - 18*kt*m*vx^2*x^2*y + 18*kt*m*vy^2*x^2*y + 6*kt*r23*x^2*y^2*ze1 - 6*m*r23*x^2*y^2*ze2 + 36*m^2*vx*vy^2*x*y + 24*kt*m*vx*vy*x^3 - 4*m*p*r12*x*y^3*ze1 - 4*m*p*r12*x^3*y*ze1 + 4*m*q*r11*x*y^3*ze1 + 4*m*q*r11*x^3*y*ze1 - 18*m*r13*vx*x^2*y*ze1 + 18*m*r23*vy*x^2*y*ze1 + 6*m*p*r22*x^2*y^2*ze1 - 6*m*q*r21*x^2*y^2*ze1)/(m^2*(x^2 + y^2)^3) - (6*x*(kt^2*vy*x^5 - kt^2*vx*y^5 - 2*m^2*vy^3*x^3 + 2*m^2*vx^3*y^3 + 6*m^2*vx^2*vy*x^3 - 2*kt^2*vx*x^2*y^3 + 2*kt^2*vy*x^3*y^2 - 6*m^2*vx*vy^2*y^3 - 6*m^2*vx^3*x^2*y + 6*m^2*vy^3*x*y^2 + kt*r23*x^5*ze1 - kt*r13*y^5*ze1 - m*r23*x^5*ze2 + m*r13*y^5*ze2 - kt^2*vx*x^4*y + kt^2*vy*x*y^4 + m*p*r22*x^5*ze1 - m*p*r12*y^5*ze1 - m*q*r21*x^5*ze1 + m*q*r11*y^5*ze1 + 3*m*r23*vx*x^4*ze1 + 3*m*r13*vy*x^4*ze1 - kt*r13*x^4*y*ze1 + kt*r23*x*y^4*ze1 - 3*m*r23*vx*y^4*ze1 - 3*m*r13*vy*y^4*ze1 + m*r13*x^4*y*ze2 - m*r23*x*y^4*ze2 - 6*kt*m*vx^2*x*y^3 - 6*kt*m*vx^2*x^3*y + 6*kt*m*vy^2*x*y^3 + 6*kt*m*vy^2*x^3*y - 2*kt*r13*x^2*y^3*ze1 + 2*kt*r23*x^3*y^2*ze1 + 2*m*r13*x^2*y^3*ze2 - 2*m*r23*x^3*y^2*ze2 + 18*m^2*vx*vy^2*x^2*y - 18*m^2*vx^2*vy*x*y^2 + 6*kt*m*vx*vy*x^4 - 6*kt*m*vx*vy*y^4 - m*p*r12*x^4*y*ze1 + m*p*r22*x*y^4*ze1 + m*q*r11*x^4*y*ze1 - m*q*r21*x*y^4*ze1 - 6*m*r13*vx*x*y^3*ze1 - 6*m*r13*vx*x^3*y*ze1 + 6*m*r23*vy*x*y^3*ze1 + 6*m*r23*vy*x^3*y*ze1 - 2*m*p*r12*x^2*y^3*ze1 + 2*m*p*r22*x^3*y^2*ze1 + 2*m*q*r11*x^2*y^3*ze1 - 2*m*q*r21*x^3*y^2*ze1))/(m^2*(x^2 + y^2)^4)) - vy*((kt^2*vx*x^4 + 5*kt^2*vx*y^4 + 6*m^2*vx^3*x^2 - 6*m^2*vx^3*y^2 - 18*m^2*vx*vy^2*x^2 + 6*kt^2*vx*x^2*y^2 + 18*m^2*vx*vy^2*y^2 + kt*r13*x^4*ze1 + 5*kt*r13*y^4*ze1 - m*r13*x^4*ze2 - 5*m*r13*y^4*ze2 + 6*kt*m*vx^2*x^3 - 6*kt*m*vy^2*x^3 - 4*kt^2*vy*x*y^3 - 4*kt^2*vy*x^3*y - 12*m^2*vy^3*x*y + m*p*r12*x^4*ze1 - m*q*r11*x^4*ze1 + 5*m*p*r12*y^4*ze1 - 5*m*q*r11*y^4*ze1 + 6*m*r13*vx*x^3*ze1 - 6*m*r23*vy*x^3*ze1 - 4*kt*r23*x*y^3*ze1 - 4*kt*r23*x^3*y*ze1 + 12*m*r23*vx*y^3*ze1 + 12*m*r13*vy*y^3*ze1 + 4*m*r23*x*y^3*ze2 + 4*m*r23*x^3*y*ze2 + 18*kt*m*vx^2*x*y^2 - 18*kt*m*vy^2*x*y^2 + 6*kt*r13*x^2*y^2*ze1 - 6*m*r13*x^2*y^2*ze2 + 36*m^2*vx^2*vy*x*y + 24*kt*m*vx*vy*y^3 - 4*m*p*r22*x*y^3*ze1 - 4*m*p*r22*x^3*y*ze1 + 4*m*q*r21*x*y^3*ze1 + 4*m*q*r21*x^3*y*ze1 + 18*m*r13*vx*x*y^2*ze1 - 18*m*r23*vy*x*y^2*ze1 + 6*m*p*r12*x^2*y^2*ze1 - 6*m*q*r11*x^2*y^2*ze1)/(m^2*(x^2 + y^2)^3) + (6*y*(kt^2*vy*x^5 - kt^2*vx*y^5 - 2*m^2*vy^3*x^3 + 2*m^2*vx^3*y^3 + 6*m^2*vx^2*vy*x^3 - 2*kt^2*vx*x^2*y^3 + 2*kt^2*vy*x^3*y^2 - 6*m^2*vx*vy^2*y^3 - 6*m^2*vx^3*x^2*y + 6*m^2*vy^3*x*y^2 + kt*r23*x^5*ze1 - kt*r13*y^5*ze1 - m*r23*x^5*ze2 + m*r13*y^5*ze2 - kt^2*vx*x^4*y + kt^2*vy*x*y^4 + m*p*r22*x^5*ze1 - m*p*r12*y^5*ze1 - m*q*r21*x^5*ze1 + m*q*r11*y^5*ze1 + 3*m*r23*vx*x^4*ze1 + 3*m*r13*vy*x^4*ze1 - kt*r13*x^4*y*ze1 + kt*r23*x*y^4*ze1 - 3*m*r23*vx*y^4*ze1 - 3*m*r13*vy*y^4*ze1 + m*r13*x^4*y*ze2 - m*r23*x*y^4*ze2 - 6*kt*m*vx^2*x*y^3 - 6*kt*m*vx^2*x^3*y + 6*kt*m*vy^2*x*y^3 + 6*kt*m*vy^2*x^3*y - 2*kt*r13*x^2*y^3*ze1 + 2*kt*r23*x^3*y^2*ze1 + 2*m*r13*x^2*y^3*ze2 - 2*m*r23*x^3*y^2*ze2 + 18*m^2*vx*vy^2*x^2*y - 18*m^2*vx^2*vy*x*y^2 + 6*kt*m*vx*vy*x^4 - 6*kt*m*vx*vy*y^4 - m*p*r12*x^4*y*ze1 + m*p*r22*x*y^4*ze1 + m*q*r11*x^4*y*ze1 - m*q*r21*x*y^4*ze1 - 6*m*r13*vx*x*y^3*ze1 - 6*m*r13*vx*x^3*y*ze1 + 6*m*r23*vy*x*y^3*ze1 + 6*m*r23*vy*x^3*y*ze1 - 2*m*p*r12*x^2*y^3*ze1 + 2*m*p*r22*x^3*y^2*ze1 + 2*m*q*r11*x^2*y^3*ze1 - 2*m*q*r21*x^3*y^2*ze1))/(m^2*(x^2 + y^2)^4)) + (ze2*(kt*r23*x^5 - kt*r13*y^5 + m*p*r22*x^5 - m*p*r12*y^5 - m*q*r21*x^5 + m*q*r11*y^5 + 3*m*r23*vx*x^4 + 3*m*r13*vy*x^4 - kt*r13*x^4*y + kt*r23*x*y^4 - 3*m*r23*vx*y^4 - 3*m*r13*vy*y^4 - 2*kt*r13*x^2*y^3 + 2*kt*r23*x^3*y^2 - 6*m*r13*vx*x*y^3 - 6*m*r13*vx*x^3*y + 6*m*r23*vy*x*y^3 + 6*m*r23*vy*x^3*y - 2*m*p*r12*x^2*y^3 + 2*m*p*r22*x^3*y^2 + 2*m*q*r11*x^2*y^3 - 2*m*q*r21*x^3*y^2 - m*p*r12*x^4*y + m*p*r22*x*y^4 + m*q*r11*x^4*y - m*q*r21*x*y^4))/(m^2*(x^2 + y^2)^3) - ((p*r22 - q*r21)*(kt*x^5*ze1 - m*x^5*ze2 + 3*m*vx*x^4*ze1 + kt*x*y^4*ze1 - 3*m*vx*y^4*ze1 - m*x*y^4*ze2 + 2*kt*x^3*y^2*ze1 - 2*m*x^3*y^2*ze2 + 6*m*vy*x*y^3*ze1 + 6*m*vy*x^3*y*ze1))/(m^2*(x^2 + y^2)^3) + ((p*r12 - q*r11)*(kt*y^5*ze1 - m*y^5*ze2 - 3*m*vy*x^4*ze1 + kt*x^4*y*ze1 + 3*m*vy*y^4*ze1 - m*x^4*y*ze2 + 2*kt*x^2*y^3*ze1 - 2*m*x^2*y^3*ze2 + 6*m*vx*x*y^3*ze1 + 6*m*vx*x^3*y*ze1))/(m^2*(x^2 + y^2)^3) - (((kt*vy)/m + (r23*ze1)/m)*(kt^2*x^5 + kt^2*x*y^4 + 6*m^2*vx^2*x^3 - 6*m^2*vy^2*x^3 + 2*kt^2*x^3*y^2 - 18*m^2*vx^2*x*y^2 + 18*m^2*vy^2*x*y^2 + 6*kt*m*vx*x^4 - 6*kt*m*vx*y^4 + 3*m*r13*x^4*ze1 - 3*m*r13*y^4*ze1 - 12*m^2*vx*vy*y^3 + 6*m*r23*x*y^3*ze1 + 6*m*r23*x^3*y*ze1 + 36*m^2*vx*vy*x^2*y + 12*kt*m*vy*x*y^3 + 12*kt*m*vy*x^3*y))/(m^2*(x^2 + y^2)^3) + (((kt*vx)/m + (r13*ze1)/m)*(kt^2*y^5 + kt^2*x^4*y + 2*kt^2*x^2*y^3 - 6*m^2*vx^2*y^3 + 6*m^2*vy^2*y^3 + 18*m^2*vx^2*x^2*y - 18*m^2*vy^2*x^2*y - 6*kt*m*vy*x^4 + 6*kt*m*vy*y^4 - 3*m*r23*x^4*ze1 + 3*m*r23*y^4*ze1 - 12*m^2*vx*vy*x^3 + 6*m*r13*x*y^3*ze1 + 6*m*r13*x^3*y*ze1 + 36*m^2*vx*vy*x*y^2 + 12*kt*m*vx*x*y^3 + 12*kt*m*vx*x^3*y))/(m^2*(x^2 + y^2)^3) + ((p*r23 - r*r21)*(m*p*x^5*ze1 + m*p*x*y^4*ze1 + 2*m*p*x^3*y^2*ze1))/(m^2*(x^2 + y^2)^3) - ((p*r13 - r*r11)*(m*p*y^5*ze1 + m*p*x^4*y*ze1 + 2*m*p*x^2*y^3*ze1))/(m^2*(x^2 + y^2)^3) + ((q*r23 - r*r22)*(m*q*x^5*ze1 + m*q*x*y^4*ze1 + 2*m*q*x^3*y^2*ze1))/(m^2*(x^2 + y^2)^3) - ((q*r13 - r*r12)*(m*q*y^5*ze1 + m*q*x^4*y*ze1 + 2*m*q*x^2*y^3*ze1))/(m^2*(x^2 + y^2)^3) + ((kr*q + j11*p*r - j33*p*r)*(m*r21*x^5*ze1 - m*r11*y^5*ze1 - m*r11*x^4*y*ze1 + m*r21*x*y^4*ze1 - 2*m*r11*x^2*y^3*ze1 + 2*m*r21*x^3*y^2*ze1))/(j22*m^2*(x^2 + y^2)^3) - ((kr*p - j22*q*r + j33*q*r)*(m*r22*x^5*ze1 - m*r12*y^5*ze1 - m*r12*x^4*y*ze1 + m*r22*x*y^4*ze1 - 2*m*r12*x^2*y^3*ze1 + 2*m*r22*x^3*y^2*ze1))/(j11*m^2*(x^2 + y^2)^3);
    eta2_1 = r - 0;
    Lg1P2 = 0;
    Lg2P2 = 0;
    Lg3P2 = 0;
    Lg4P2 = 1/j33;
    LfP2 = -(kr*r - j11*p*q + j22*p*q)/j33;
    

% xi1_1 = x - cos(y);
% xi1_2 = vx + vy*sin(y);
% xi1_3 = -(kt*vx + r13*ze1 + kt*vy*sin(y) + r23*ze1*sin(y) - m*vy^2*cos(y))/m;
% xi1_4 = (kt*((kt*vx)/m + (r13*ze1)/m))/m - (ze2*(r13 + r23*sin(y)))/m + (ze1*(p*r12 - q*r11))/m - (vy*(kt*vy*cos(y) + r23*ze1*cos(y) + m*vy^2*sin(y)))/m + ((kt*sin(y) - 2*m*vy*cos(y))*((kt*vy)/m + (r23*ze1)/m))/m + (ze1*sin(y)*(p*r22 - q*r21))/m;
% Lg1Lf3S1 = -(r13 + r23*sin(y))/m;
% Lg2Lf3S1 = ((r12*ze1)/m + (r22*ze1*sin(y))/m)/j11;
% Lg3Lf3S1 = -((r11*ze1)/m + (r21*ze1*sin(y))/m)/j22;
% Lg4Lf3S1 = 0;
% Lf4S1 = (p*r22 - q*r21)*((ze2*sin(y))/m - (ze1*(kt*sin(y) - 2*m*vy*cos(y)))/m^2 + (vy*ze1*cos(y))/m) + ((kt*vy)/m + (r23*ze1)/m)*((kt*vy*cos(y) + r23*ze1*cos(y) + m*vy^2*sin(y))/m + 2*cos(y)*((kt*vy)/m + (r23*ze1)/m) - (kt*(kt*sin(y) - 2*m*vy*cos(y)))/m^2 + (vy*(kt*cos(y) + 2*m*vy*sin(y)))/m) + ze2*((p*r12 - q*r11)/m + (kt*r13)/m^2 + (r23*(kt*sin(y) - 2*m*vy*cos(y)))/m^2 + (sin(y)*(p*r22 - q*r21))/m - (r23*vy*cos(y))/m) + (ze2/m - (kt*ze1)/m^2)*(p*r12 - q*r11) + vy*((vy*(kt*vy*sin(y) + r23*ze1*sin(y) - m*vy^2*cos(y)))/m + ((kt*cos(y) + 2*m*vy*sin(y))*((kt*vy)/m + (r23*ze1)/m))/m + (ze1*cos(y)*(p*r22 - q*r21))/m - (r23*ze2*cos(y))/m) - (kt^2*((kt*vx)/m + (r13*ze1)/m))/m^2 + (((r11*ze1)/m + (r21*ze1*sin(y))/m)*(kr*q + j11*p*r - j33*p*r))/j22 - (((r12*ze1)/m + (r22*ze1*sin(y))/m)*(kr*p - j22*q*r + j33*q*r))/j11 + (p*ze1*(p*r13 - r*r11))/m + (q*ze1*(q*r13 - r*r12))/m + (p*ze1*sin(y)*(p*r23 - r*r21))/m + (q*ze1*sin(y)*(q*r23 - r*r22))/m;
% xi2_1 = z - 10;
% xi2_2 = vz;
% xi2_3 = -(kt*vz - g*m + r33*ze1)/m;
% xi2_4 = (ze1*(p*r32 - q*r31))/m - (r33*ze2)/m + (kt*((kt*vz)/m - g + (r33*ze1)/m))/m;
% Lg1Lf3S2 = -r33/m;
% Lg2Lf3S2 = (r32*ze1)/(j11*m);
% Lg3Lf3S2 = -(r31*ze1)/(j22*m);
% Lg4Lf3S2 = 0;
% Lf4S2 = ze2*((p*r32 - q*r31)/m + (kt*r33)/m^2) + (ze2/m - (kt*ze1)/m^2)*(p*r32 - q*r31) - (kt^2*((kt*vz)/m - g + (r33*ze1)/m))/m^2 + (p*ze1*(p*r33 - r*r31))/m + (q*ze1*(q*r33 - r*r32))/m + (r31*ze1*(kr*q + j11*p*r - j33*p*r))/(j22*m) - (r32*ze1*(kr*p - j22*q*r + j33*q*r))/(j11*m);
% eta1_1 = y;
% eta1_2 = vy;
% eta1_3 = -(kt*vy + r23*ze1)/m;
% eta1_4 = (kt^2*vy + kt*r23*ze1 - m*r23*ze2 + m*p*r22*ze1 - m*q*r21*ze1)/m^2;
% Lg1Lf3P1 = -r23/m;
% Lg2Lf3P1 = (r22*ze1)/(j11*m);
% Lg3Lf3P1 = -(r21*ze1)/(j22*m);
% Lg4Lf3P1 = 0;
% Lf4P1 = (ze2*(kt*r23 + m*p*r22 - m*q*r21))/m^2 - (kt^2*((kt*vy)/m + (r23*ze1)/m))/m^2 - ((p*r22 - q*r21)*(kt*ze1 - m*ze2))/m^2 + (p*ze1*(p*r23 - r*r21))/m + (q*ze1*(q*r23 - r*r22))/m + (r21*ze1*(kr*q + j11*p*r - j33*p*r))/(j22*m) - (r22*ze1*(kr*p - j22*q*r + j33*q*r))/(j11*m);
% eta2_1 = r - 2;
% Lg1P2 = 0;
% Lg2P2 = 0;
% Lg3P2 = 0;
% Lg4P2 = 1/j33;
% LfP2 = -(kr*r - j11*p*q + j22*p*q)/j33;

      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    D = [Lg1Lf3S1 Lg2Lf3S1 Lg3Lf3S1  Lg4Lf3S1; 
     Lg1Lf3S2 Lg2Lf3S2  Lg3Lf3S2 Lg4Lf3S2; 
     Lg1Lf3P1 Lg2Lf3P1  Lg3Lf3P1 Lg4Lf3P1; 
     Lg1P2  Lg2P2   Lg3P2  Lg4P2];
        
    %inv_D=inv(D);
    %det_D(i)=det(D);
    
    v1_tran=  -Lf4S1 -k1*xi1_1 - k2*xi1_2 - k3*xi1_3 - k4*xi1_4;
    v2_tran=  -Lf4S2 -k5*xi2_1 - k6*xi2_2 - k7*xi2_3 - k8*xi2_4;
    v1_tang = -Lf4P1  -k10*(eta1_2 - 1) -k11*eta1_3 -k12*eta1_4;
    v2_tang = -LfP2 -k13*(eta2_1);

    
    u=D\[v1_tran; v2_tran; v1_tang; v2_tang];
    
    u_new=u(1);
    taup=u(2);
    tauq=u(3);
    taur=u(4);

    
    up = taup;
    uq = tauq;
    ur = taur;
    

    Omega = [p;q;r];
    %%%% System Dynamics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     x =                                      x + dt*vx;
%     y =                                      y + dt*vy;
%     z =                                      z + dt*vz;
%     vx =               vx - dt*((kt*vx)/m + (r13*ze1)/m);
%     vy =              vy - dt*((kt*vy)/m + (r23*ze1)/m);
%     vz =          vz - dt*((kt*vz)/m - g + (r33*ze1)/m);
%     r11 =                       r11 - dt*(q*r13 - r*r12);
%     r21 =                       r21 - dt*(q*r23 - r*r22);
%     r31 =                       r31 - dt*(q*r33 - r*r32);
%     r12 =                       r12 + dt*(p*r13 - r*r11);
%     r22 =                       r22 + dt*(p*r23 - r*r21);
%     r32 =                       r32 + dt*(p*r33 - r*r31);
%     r13 =                       r13 - dt*(p*r12 - q*r11);
%     r23 =                       r23 - dt*(p*r22 - q*r21);
%     r33 =                       r33 - dt*(p*r32 - q*r31);
%     p = p - dt*((kr*p - j22*q*r + j33*q*r)/j11 - taup/j11);
%     q = q - dt*((kr*q + j11*p*r - j33*p*r)/j22 - tauq/j22);
%     r = r - dt*((kr*r - j11*p*q + j22*p*q)/j33 - taur/j33);
%     ze1 =                                    ze1 + dt*ze2;
%     ze2 =                                    ze2 + dt*u_new;
%     
%      %%% need to construct R for plotting purposes
%     R = [r11 r12 r13; r21 r22 r23;r31 r32 r33];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%% New system dynamics in exact form %%%%%%%%%%%
    x =                                      x + dt*vx;
    y =                                      y + dt*vy;
    z =                                      z + dt*vz;
    vx =               vx - dt*((kt*vx)/m + (r13*ze1)/m);
    vy =              vy - dt*((kt*vy)/m + (r23*ze1)/m);
    vz =          vz - dt*((kt*vz)/m - g + (r33*ze1)/m);
    %R = R + dt(R*so3_hat(Omega));
    R = R + dt.*(R*so3_hat(Omega));
    %%% Find Nearest Orthogonal Matrix to make sure that det(R) = 1 or R remains in
    %%% SO(3)
    H = logm(R);
    H_tilda = (1/2)*(H - H');
    R_tilda = expm(H_tilda);
    R = R_tilda;    
%   R = R*expm(dt*so3_hat(Omega));
    p = p - dt*((kr*p - j22*q*r + j33*q*r)/j11 - taup/j11);
    q = q - dt*((kr*q + j11*p*r - j33*p*r)/j22 - tauq/j22);
    r = r - dt*((kr*r - j11*p*q + j22*p*q)/j33 - taur/j33);
    ze1 =                                    ze1 + dt*ze2;
    ze2 =                                    ze2 + dt*u_new;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    r11 = R(1,1);
    r12 = R(1,2);
    r13 = R(1,3);
    r21 = R(2,1);
    r22 = R(2,2);
    r23 = R(2,3);
    r31 = R(3,1);
    r32 = R(3,2);
    r33 = R(3,3);
    
    State = [x;y;z;vx;vy;vz;r11;r12,;r13;r21;r22;r23;r31;r32;r33;p;q;r;ze1;ze2];
    if (noise == 1)
        [State] = ips_callback(State);
        [State] = imu_callback(State);
    end
    det(R);
    Omega = [p;q;r];

   [ph,th,ps] = decompose_rotation(R);
    
%  error_function(i) = norm(e_R);

    %det_R(i) = det(R);
    R_d = eye(3);
    %det_R_d(i) = det(R_d);
    
    %trace_R(i) = trace(R);
    
    %error(i) = trace( eye(3) - R_d'*R);
    
    error_R = eye(3) - R_d'*R;
    %normed_error_R(i) = norm(error_R) ;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%% saving data for plotting purposes %%%%%%%%%%%%%%%%%%
    ph_graph(i) = ph;
    th_graph(i) = th;
    ps_graph(i) = ps;
    
    p_graph(i) = Omega(1);
    q_graph(i) = Omega(2);
    r_graph(i) = Omega(3);
    x_graph(i) = x;
    y_graph(i) = y;
    z_graph(i) = z;
    vx_graph(i) = vx;
    vy_graph(i) = vy;
    vz_graph(i) = vz;
    
    xi1_1_graph(i) = xi1_1;
    xi1_2_graph(i) = xi1_2;
    xi1_3_graph(i) = xi1_3;
    xi1_4_graph(i) = xi1_4;
    
    xi2_1_graph(i) = xi2_1;
    xi2_2_graph(i) = xi2_2;
    xi2_3_graph(i) = xi2_3;
    xi2_4_graph(i) = xi2_4;
    
    eta1_1_graph(i) = eta1_1;
    eta1_2_graph(i) = eta1_2;
    eta1_3_graph(i) = eta1_3;
    eta1_4_graph(i) = eta1_4;
    
    eta2_1_graph(i) = eta2_1;
   
    %%%%%  control input storage %%%%%%%%
    up_graph(i) = up;
    uq_graph(i) = uq;
    ur_graph(i) = ur;
    ut_graph(i) = ze1;
    
    
end

% %%% displaying the initial marker
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
%     plot3(sin(lambda),sin(2*cos(lambda)),cos(lambda), '--r','color','green','linewidth',1);
%     hold on;
%     
%     plot3(x,y,z, 'r','color','red','linewidth',2);
%     
%     axis([-2 2 -2 2 -1 4]);
%     %title('Quadrotor Following a unit circle elevated at a height of 3-units')
%     xlabel('x_{1} = x')
%     ylabel('x_{3} = y')
%     zlabel('x_{5} = z')
%     grid on;
%     
%     plot3(X1_0(k), X2_0(k), X3_0(k), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');

lambda = 0:0.01:2*pi;
ss = size(lambda);
z_lambda = zeros(1,ss(2)) + 1;
figure;

figure()
hold on;
plot3(x_graph(1),y_graph(1),z_graph(1), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'red');
plot3(sin(lambda),cos(lambda), (lambda.*0 + 10), '--r','color','green','linewidth',3);
plot3(x_graph(1:end-2),y_graph(1:end-2),z_graph(1:end-2), 'r','color','red','linewidth',2);
% axis([-2 2 -2 2 -1 12]);
legend({'$\chi(0)$','$\gamma$','$h(\chi)$'},'FontSize',14,'Interpreter','latex')
xlabel('$x(m)$','FontSize',16,'Interpreter','latex')
ylabel('$y(m)$','FontSize',16,'Interpreter','latex')
zlabel('$z(m)$','FontSize',16,'Interpreter','latex')
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%% xi plots %%%%%%%%%%%%%%%%%%%%%%%%%%%
figure()
subplot(3,1,1)
hold on;
plot(time(1:end-2),xi1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi1_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\xi_{1i}$','FontSize',16,'Interpreter','latex')
legend({'$\xi_{11}$','$\xi_{12}$','$\xi_{13}$','$\xi_{14}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;

subplot(3,1,2)

hold on;
plot(time(1:end-2),xi2_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi2_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi2_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi2_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\xi_{2i}$','FontSize',16,'Interpreter','latex')
legend({'$\xi_{21}$','$\xi_{22}$','$\xi_{23}$','$\xi_{24}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;

subplot(3,1,3)
hold on;
plot(time(1:end-2),eta1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),eta1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),eta1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),eta1_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\eta_{1i}$','FontSize',16,'Interpreter','latex')
legend({'$\eta_{11}$','$\eta_{12}$','$\eta_{13}$','$\eta_{14}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
% legend('$\hat{\psi}$','Interpreter','latex') % R2018b and later
grid on;



figure()
hold on;
subplot(3,1,1)
plot(time,vx_graph, 'r','color','blue','linewidth',2)
xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
ylabel('$v_x (m/sec)$','FontSize',14,'Interpreter','latex')
grid on;
subplot(3,1,2)
plot(time,vy_graph, 'r','color','green','linewidth',2);
xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
ylabel('$v_y(m/sec)$','FontSize',14,'Interpreter','latex')
grid on;
subplot(3,1,3)
plot(time,vz_graph, 'r','color','red','linewidth',2);
xlabel('$t(sec)$','FontSize',14,'Interpreter','latex')
ylabel('$v_z(m/sec)$','FontSize',14,'Interpreter','latex')
grid on;






figure()
plot3(x_graph(1:end-2),y_graph(1:end-2),z_graph(1:end-2), 'r','color','red','linewidth',2);

figure()
hold on;
plot(time(1:end-2),xi1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi1_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\xi_{1i}$','FontSize',16,'Interpreter','latex')
grid on;


figure()
hold on;
plot(time(1:end-2),xi2_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),xi2_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),xi2_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),xi2_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\xi_{2i}$','FontSize',16,'Interpreter','latex')
grid on;

figure()
hold on;
plot(time(1:end-2),eta1_1_graph(1:end-2), 'r','color','red','linewidth',2)
plot(time(1:end-2),eta1_2_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),eta1_3_graph(1:end-2), 'r','color','green','linewidth',2)
plot(time(1:end-2),eta1_4_graph(1:end-2), 'r','color','black','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\eta_{1i}$','FontSize',16,'Interpreter','latex')
grid on;

figure()
hold on;
plot(time(1:end-2),eta2_1_graph(1:end-2), 'r','color','red','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\eta_{21}$','FontSize',16,'Interpreter','latex')
grid on;

% figure();
% plot(trace_R);
% title('trace(R)')
% grid on;
% 
% figure();
% plot(det_D);
% title('det(D)')
% grid on;

% figure();
% plot(decoupling_condition);
% title('trace (R_d*R)')
% grid on;

% figure()
% plot(time(1:end-2),error, 'r','color','green','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$tr(I-R_dR)$','FontSize',16,'Interpreter','latex')
% grid on;
% 
% 
% figure()
% plot(time(1:end-2),det_R, 'r','color','green','linewidth',2)
% xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
% ylabel('$det(R)$','FontSize',16,'Interpreter','latex')
% grid on;

%%

figure()
hold on;
plot(time(1:end-2),ph_graph(1:end-2)*(180/pi), 'r','color','blue','linewidth',2)
plot(time(1:end-2),th_graph(1:end-2)*(180/pi), 'r','color','green','linewidth',2);
plot(time(1:end-2),ps_graph(1:end-2)*(180/pi), 'r','color','red','linewidth',2);
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\phi,\theta, \psi (degree) $','FontSize',16,'Interpreter','latex')
legend('\phi','\theta', '\psi');
grid on;

figure()
hold on;
plot(time(1:end-2),p_graph(1:end-2), 'r','color','blue','linewidth',2)
plot(time(1:end-2),q_graph(1:end-2), 'r','color','green','linewidth',2);
plot(time(1:end-2),r_graph(1:end-2), 'r','color','red','linewidth',2);
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\Omega (rad/sec)$','FontSize',16,'Interpreter','latex')
legend({'$p$','$q$','$r$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
grid on;


figure()
subplot(2,1,1)
hold on;
plot(time(1:end-2),ut_graph(1:end-2), 'r','color','blue','linewidth',2)
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$u_t$(N)','FontSize',16,'Interpreter','latex')
legend({'$u_{t}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
grid on;

hh = 1/25*(ones(1,5));
fil_ur_graph = conv(hh,ur_graph);

subplot(2,1,2)
hold on;
plot(time(1:end-2),up_graph(1:end-2), 'r','color','green','linewidth',2);
plot(time(1:end-2),uq_graph(1:end-2), 'r','color','red','linewidth',2);
plot(time(1:end-2),fil_ur_graph(1:end-6), 'r','color','black','linewidth',2);
xlabel('$t(sec)$','FontSize',16,'Interpreter','latex')
ylabel('$\tau$(N-m)','FontSize',16,'Interpreter','latex')
legend({'$\tau_{p}$','$\tau_{q}$','$\tau_{r}$'},'FontSize',12,'Interpreter','latex') % R2018a and earlier
grid on;

%% For animation

x = x_graph(1:end-2);
y = y_graph(1:end-2);
z = z_graph(1:end-2);
phi = ph_graph(1:end-2);
theta = th_graph(1:end-2);
psi = ps_graph(1:end-2);

animatesimulation


